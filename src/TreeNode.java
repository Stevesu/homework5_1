import java.util.*;

public class TreeNode{
    private String name;
    private TreeNode firstChild;
    private TreeNode nextSibling;

    /**
     * Constructor of the TreeNode
     * @param n as string. Name of the node.
     * @param d as treenode. First child of the node.
     * @param r as treenode. Next sibling of the node.
     */
    TreeNode(String n, TreeNode d, TreeNode r) {
        this.setName(n);
        this.setFirstChild(d);
        this.setNextSibling(r);
    }

    /**
     * Abiklassid puu käsitlemiseks saadud Jaanus Pöial longumaterjalidest: http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
     */

    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setNextSibling(TreeNode p) {
        nextSibling = p;
    }

    public TreeNode getNextSibling() {
        return nextSibling;
    }

    public void setFirstChild(TreeNode a) {
        firstChild = a;
    }

    public TreeNode getFirstChild() {
        return firstChild;
    }

    /**
     * Convert a TreeNode to string.
     * @return s as String.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        this.stringBuilder(s);
        return s.toString();
    }

    /**
     * Recursive class for converting a treenode to string.
     * @param s as stringbuilder.
     */
    private void stringBuilder(StringBuilder s) {
        s.append(this.getName());
        if (!this.isLeaf() && this.firstChild.name.length() > 0) {
            s.append("(");
            this.firstChild.stringBuilder(s);
        }
        if (this.hasNext()) {
            s.append(",");
            this.getNextSibling().stringBuilder(s);
        } else if (!this.hasNext() && (count(s.toString(), "(") - count(s.toString(), ")") == 1)) {
            s.append(")");
        }
    }

    public boolean hasNext() {
        return (getNextSibling() != null);
    }

    public TreeNode next() {
        return getNextSibling();
    }

    public TreeNode children() {
        return getFirstChild();
    }

    public void addChild(TreeNode a) {
        if (a == null) return;
        TreeNode children = children();
        if (children == null)
            setFirstChild(a);
        else {
            while (children.hasNext())
                children = children.next();
            children.setNextSibling(a);
        }
    }

    public boolean isLeaf() {
        return (getFirstChild() == null);
    }

    /**
     * Construct a tree from the input string s.
     * @param s as string representation of the tree.
     * @return node as TreeNode.
     */
    public static TreeNode parsePrefix(String s) {
        //Handle incorrect input
        handleInput(s);

        //Find the number of Nodes on the current level
        int numberOfNodes = countNodes(s);

        //Determine the "name" of the current Node
        String nodeName = s;
        if (s.indexOf('(') > 0) nodeName = s.substring(0, s.indexOf('('));

        //Create a root
        TreeNode node = new TreeNode(nodeName, null, null);
        String temp = s.substring(s.indexOf('(') + 1, closingParentheses(s, s.indexOf('(')));

        if (numberOfNodes > 1) {
            int endIndexOfFirstNode = 0;
            int counter1 = 0;
            for (int i = 0; i < temp.length(); i++) {
                char c = temp.charAt(i);
                if (c == '(') counter1++;
                if (c == ')') counter1--;
                if (c == ',' && counter1 == 0) {
                    endIndexOfFirstNode = i;
                    break;
                }
            }
            String firstNode = temp.substring(0, endIndexOfFirstNode);
            node.addChild(new TreeNode(firstNode, null, null));
            if (firstNode.contains("(")) parsePrefix(firstNode);
        } else if (numberOfNodes == 1) {
            node.addChild(new TreeNode(temp, null, null));
            if(temp.contains("(")) parsePrefix(temp);
        }

        //Determine "nextSiblings" of the current Node
        List<Integer> secondSiblingStartingPoint = new ArrayList();
        int counter2 = 0;
        for (int i = 0; i < temp.length(); i++) {
            char c = temp.charAt(i);
            if (c == '(') counter2++;
            else if (c == ')') counter2--;
            else if (c == ',' && counter2 == 0) secondSiblingStartingPoint.add(i);
        }

        if (numberOfNodes > 1) {
            for (int i = 0; i < numberOfNodes - 1; i++) {
                if (i < numberOfNodes - 2) {
                    String nextSibling = temp.substring(secondSiblingStartingPoint.get(i) + 1, secondSiblingStartingPoint.get(i + 1));
                    node.addChild(new TreeNode(nextSibling, null, null));
                    if (nextSibling.contains("(")) parsePrefix(nextSibling);
                } else {
                    String nextSibling = temp.substring(secondSiblingStartingPoint.get(i) + 1, temp.length());
                    node.addChild(new TreeNode(nextSibling, null, null));
                    if (nextSibling.contains("(")) parsePrefix(nextSibling);
                }
            }
        }
        return node;
    }

    /**
     * Class for checking the input correctness.
     * @param s as String.
     */
    private static void handleInput(String s) {
        List <Integer> nPos= listPositions(s, '(');
        List <Integer> cPos = listPositions(s, ',');
        List <Integer> mPos = listPositions(s, ')');

        if (s.equals("\t") || s.trim().equals(null) || s.trim().equals("")) throw new RuntimeException("Vigane sisend. Sisend on tühi");
        if (s.contains(" ")) throw new RuntimeException("Vigane sisend. Tühik nimes");
        if (count(s, "(") != count(s, ")")) throw new RuntimeException("Vigane sisend. Sulgusid on puudu");
        for (int i = 0; i < count(s, "("); i++) {
            int r = closingParentheses(s, nPos.get(i));
            if (r - nPos.get(i) == 1) throw new RuntimeException("Vigane sisend. Tühjad sulud");
            if (i > 0 && nPos.get(i)-nPos.get(i-1)==1 && closingParentheses(s, nPos.get(i-1))-r == 1) throw new RuntimeException("Vigane sisend. Topelt sulud");
        }
        for (int i = 0; i < cPos.size(); i++) {
            if (i > 0 && cPos.get(i)-cPos.get(i-1)==1) throw new RuntimeException("Vigane sisend. Topelt koma");
        }
        if (cPos.size() > 0 && mPos.size() > 0 && (cPos.get(cPos.size()-1) > mPos.get((mPos.size()-1)))) throw new RuntimeException("Vigane sisend. Valesti paigutatud komad");
        if (cPos.size() > 0 && nPos.size() > 0 && (cPos.get(0) < nPos.get(0))) throw new RuntimeException("Vigane sisend. Valesti paigutatud komad");
        int commaCounter = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') commaCounter ++;
            if (c == ')') commaCounter --;
            if (commaCounter == 0 && c == ',') throw new RuntimeException("Vigane sisned. Valesti paigutatud komad");
        }
        for (Integer nPo : nPos) {
            if (s.charAt(nPo + 1) == ',') throw new RuntimeException("Vigane sisend. Valesti paigutatud komad");
        }
        if (s.contains(",") && !s.contains("(")) throw new RuntimeException("Vigane sisend. Puudu sulud");
    }

    /**
     * Count the nodes on current level of the tree.
     * @param s as string representation of the tree.
     * @return n as integer. Number of nodes on the current level of the tree.
     */
    private static int countNodes(String s) {
        int n = 1; // number of Nodes on current level
        int counter = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') counter++;
            else if (c == ')') counter--;
            else if (c == ',' && counter == 1)    n++;
        }
        return n;
    }

    /**
     * Finds the position of closing parentheses for element at position i.
     * @param s as string where the parentheses are looked for.
     * @param i as integer. Index of opening parentheses.
     * @return pos as integer. Index of the closing parentheses.
     */
    private static int closingParentheses(String s, Integer i) {
        int counter = 1;
        int pos = 0;
        for (int j = i + 1; j < s.length(); j++) {
            if (s.charAt(j) == '(')counter++;
            else if (s.charAt(j) == ')')counter--;
            if (counter == 0) {
                pos = j;
                break;
            }
        }
        return pos;
    }

    /**
     * Counts the number of string s1 in string s.
     * @param s  as string.
     * @param s1 as string.
     * @return number of string s1 in string s as integer.
     */
    private static int count(String s, String s1) {
        return s.length() - s.replace(s1, "").length();
    }

    /**
     * Lists the positions of character c in string s.
     * @param s as string.
     * @param c as character.
     * @return Positions of character c in string s as list.
     */
    private static List listPositions(String s, char c) {
        List<Integer> index = new ArrayList();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c)index.add(i);
        }
        return index;
    }

    /**
     * Converts the Tree left Parenthetic Representation to right Parenthetic Representation.
     * @return b as string.
     */
    public String rightParentheticRepresentation() {
        StringBuffer b = new StringBuffer(this.toString());
        int n = count(this.toString(), "(");
        List<Integer> nPositions = listPositions(this.toString(), '(');
        List<Integer> lPositions = new ArrayList();

        for (int i = 0; i < b.length(); i++) {
            char c = b.charAt(i);
            if (c == '(' || c == ',') lPositions.add(i);
        }

        List<String> nodeName = new ArrayList();
        for (int i = 0; i < n; i++) {
            if (i == 0) nodeName.add(this.toString().substring(0, nPositions.get(i)));
            else nodeName.add(this.toString().substring(lPositions.get(lPositions.indexOf(nPositions.get(i)) - 1) + 1, nPositions.get(i)));
        }

        List<Integer> mPositions = new ArrayList();
        int currentPosition = 0;
        int minPosition  = this.toString().length();
        int addLength = 0;
        for (int i = 0; i < n; i++) {
            mPositions.add(closingParentheses(this.toString(), nPositions.get(i)));
            if (i > 0 && minPosition > currentPosition) minPosition = currentPosition;
            currentPosition = closingParentheses(this.toString(), nPositions.get(i));
            if (currentPosition > minPosition) addLength = addLength + b.toString().length() - this.toString().length()-1;
            b.insert(mPositions.get(i) + 1 + addLength, nodeName.get(i));
        }

        for (int i = 0; i < n; i++) {
            if (i == 0) b.delete(0, this.toString().indexOf('('));
            else {
                int nameStart = b.indexOf(nodeName.get(i));
                int nameEnd = nameStart + nodeName.get(i).length();
                b.delete(nameStart, nameEnd);
            }
        }
        return b.toString();
    }

    /**
     * Main method. Calls other methods.
     * @param param as string.
     */
    public static void main(String[] param) {
        String s = "A(B,C)";
        TreeNode t = TreeNode.parsePrefix(s);
        String v = t.rightParentheticRepresentation();
        System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
    }
}